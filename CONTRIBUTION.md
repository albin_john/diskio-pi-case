# How to contribute

If you want to contribute, follow these steps:

- First, create your own clone of diskio-pi-case.
- When you want to add a new feature, create a branch on your own fork following [these instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html).
- Push it to your fork of the repository and open a pull request.
- A pull request will start our continuous integration service.
  - At the end, we will perform a code review and merge your branch to the master branch.
